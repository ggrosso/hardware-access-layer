import random
import json
import logging
import sys
import argparse
import math
import ctypes
import os
import time, datetime
import resource
import tracemalloc
import gc

from prometheus_client import start_http_server, Summary, Gauge, Counter
from ReadWriteTools import rw

def memory_usage_resource():
    import resource
    rusage_denom = 1024.
    if sys.platform == 'darwin':
        # ... it seems that in OSX the output is different units ...
        rusage_denom = rusage_denom * rusage_denom
    mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / rusage_denom
    return mem

def dump_garbage(  ):
    """
    show us what the garbage is about
    """
    # Force collection
    print("\nGARBAGE:")
    gc.collect(  )

    print("\nGARBAGE OBJECTS:")
    for x in gc.garbage:
        s = str(x)
        if len(s) > 80: s = s[:77]+'...'
        print(type(x),"\n  ", s)
    return

def formatter(prog):
    return argparse.HelpFormatter(prog, max_help_position=10, width=100)

if __name__ == '__main__':
    gc.enable()
    # Start up the server to expose the metrics.  
    start_http_server(8097)
    parser = argparse.ArgumentParser( formatter_class=formatter)
    parser.add_argument('-j', '--jsonfile',  type=str,  help="config file json",         required=True)
    parser.add_argument('-l', '--logfile',   type=str,  help="logfile",                  required=False)
    parser.add_argument('-d', '--dump',      type=int,  help="dump messages",            required=False, default=0)
    parser.add_argument('-b', '--board',     type=str,  help="scouting board",           required=False)
    parser.add_argument('-e', '--errfile',   type=str,  help="debugfile",                required=False)
    parser.add_argument('-m', '--mem_debug', type=str, help="print memory tracing for debug", required=False, default=0)
    parser.add_argument('-t', '--timesleep', type=float,  help="seconds before updating",  required=False, default=5)
    args = parser.parse_args()

    timesleep = args.timesleep

    if args.logfile:
        logging.basicConfig(filename=args.logfile,
                            format='%(levelname)s:%(message)s', level=logging.INFO)
    if args.errfile:
        logging.basicConfig(filename=args.errfile,
                            format='%(levelname)s:%(message)s', level=logging.DEBUG)
                                                                          
    with open(args.jsonfile, 'r') as jsonfile:
        config_json = json.load(jsonfile)
    mem_debug = False
    if args.mem_debug:
        mem_debug = True
        tracemalloc.start()
        gc.set_debug(gc.DEBUG_LEAK)
        memory = 0
    verbose = 0
    if args.errfile: verbose = 1          
    dump   = args.dump
    board  = args.board
    del args, parser

    metrics_board_dict = {}
    previous_read_values_board = {}
    if board==None:
        logging.warning("The argument 'board' (-b) has not been specified. The monitoring process will loop over all the boards included in the address table.")
        for board in config_json:
            metrics_dict = {}
            for reg in config_json[board]["registers"]:
                metric   = config_json[board]["registers"][reg]["metric"]
                if metric == 'gauge':
                    metrics_dict[reg] = Gauge(reg, board)
                elif metric == 'counter':
                    metrics_dict[reg] = Counter(reg, board)

            metrics_board_dict[board]=metrics_dict
            previous_read_values_board[board] = {}
    else:
        metrics_dict = {}
        for reg in config_json[board]["registers"]:
            metric   = config_json[board]["registers"][reg]["metric"]
            if metric == 'gauge':
                metrics_dict[reg] = Gauge(reg, board)
            elif metric == 'counter':
                metrics_dict[reg] = Counter(reg, board)

        metrics_board_dict[board]=metrics_dict
        previous_read_values_board[board] = {}


        while True:
            # monitoring metrics
            if board==None:
                for board in config_json:
                    device = str(config_json[board]["driver"])
                    logging.info("board %s (driver %s)"%(board, device))
                    prometheus_monitoring.monitoring_board(config_json, board, metrics_board_dict[board], device, 
                                                           previous_read_values=previous_read_values_board[board], 
                                                           dump=dump, verbose=verbose, logging=logging)
                gc.collect()
            else:
                device = str(config_json[board]["driver"])
                logging.info("board %s (driver %s)"%(board, device))
                prometheus_monitoring.monitoring_board(config_json, board, metrics_board_dict[board], device, 
                                                       previous_read_values=previous_read_values_board[board], 
                                                       dump=dump, verbose=verbose, logging=logging)
                gc.collect()

            if mem_debug:
                memory_avg, memory_max= tracemalloc.get_traced_memory()
                if memory_avg > memory:
                    print(datetime.datetime.now())
                    print("Current memory usage is {memory_avg / 10**6}MB; Peak was {memory_max / 10**6}MB")
                    memory = memory_avg

            time.sleep(timesleep)
    tracemalloc.stop()
