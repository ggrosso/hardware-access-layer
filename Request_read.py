import os
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--register', type=str,  help="register",       required=True)
    parser.add_argument('-b', '--board',    type=str,  help="scouting board", required=True)
    parser.add_argument('-p', '--port',     type=str,  help="port",           required=False, default="8080")
    args = parser.parse_args()

    os.system("curl  http://127.0.0.1:%s/%s/%s/read"%(args.port, args.board, args.register))
