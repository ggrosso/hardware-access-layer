import os
import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--register', type=str,  help="register",       required=True)
    parser.add_argument('-b', '--board',    type=str,  help="scouting board", required=True)
    parser.add_argument('-p', '--port',     type=str,  help="port",           required=False, default="8080")
    parser.add_argument('-v', '--value',    type=str,  help="value",          required=True)
    args = parser.parse_args()

    os.system('curl -X POST -F "value=%s" http://127.0.0.1:%s/%s/%s/write'%(args.value, args.port, args.board, args.register))
