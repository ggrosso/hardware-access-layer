import json
import logging
import argparse

def HandleRegisters(config_json, action, board, register, width=None, offset=None, metric=None, writable=False, exposable=False, address=None, logging=None):
    if action == 'add':
        if width==None or offset==None or address==None:
            logging.error('address (-l), width (-w) and offset (-o) are required to add a new register.')
            exit()
        if register in config_json[board]['registers']:
            logging.error('register %s already configured in board %s; it cannot be added. Try action modify instead.'%(register, board))
            exit()
        if offset>31 or offset<0:
            logging.error('offset must be an integer between 0 and 31.')
            exit()
        if width>32 or width<1:
            logging.error('width must be an integer between 1 and 32.')
            exit()
        if offset+width<1 or offset+width>32:
            logging.error('the sum of offset and width must be between 1 and 32.')
            exit()

        config_json[board]['registers'][register] = {'width': width,
                                                    'address': address,
                                                    'offset': offset,
                                                     'metric': metric,
                                                     'writable': writable,
                                                     'exposable': exposable
                                                    }

    elif action == 'delete':
        if not register in config_json[board]['registers']:
            logging.error('register %s in board %s not configured yet thus it could not be removed from config file.'%(register, board))
            exit()
        config_json[board]['registers'].pop(register)

    elif action == 'modify':
        if width==None or offset==None or address==None:
            logging.error('new values for address (-l), width (-w) and offset (-o) are required to add a new register.')
            exit()
        if offset>31 or offset<0:
            logging.error('offset must be an integer between 0 and 31. Register %s was not updated.'%(register))
            exit()
        if width>32 or width<1:
            logging.error('width must be an integer between 1 and 32. Register %s was not updated.'%(register))
            exit()
        if offset+width<1 or offset+width>32:
            logging.error('the sum of offset and width must be between 1 and 32. Register %s was not updated.'%(register))
            exit()
        if not register in config_json[board]['registers']:
            logging.error('register %s in board %s not configured yet; adding it.'%(register, board))
            config_json[board]['registers'][register] = {'width': width,
                                                         'address': address,
                                                         'offset': offset,
                                                         'metric': metric, 
                                                         'writable': writable,
                                                         'exposable': exposable
                                                     }
        else:
            config_json[board]['registers'][register]['width']    = width
            config_json[board]['registers'][register]['address'] = address
            config_json[board]['registers'][register]['offset']   = offset
            config_json[board]['registers'][register]['metric']   = metric
            config_json[board]['registers'][register]['writable'] = writable
            config_json[board]['registers'][register]['exposable']= exposable
        return

actions = ['add', 'modify', 'delete']
if __name__ == '__main__':
    parser = argparse.ArgumentParser()    #Python tool that makes it easy to create an user-friendly command-line interface                                              
    parser.add_argument('-j','--jsonfile',  type=str, help="config file json", required=True)
    parser.add_argument('-b','--board',     type=str, help="scouting board",   required=True)
    parser.add_argument('-a','--action',    type=str, help="control action",   required=True, choices=actions)
    parser.add_argument('-r','--register',  type=str, help="control register", required=True)
    parser.add_argument('-w','--width',     type=int, help="new width",        required=False)
    parser.add_argument('-o','--offset',    type=int, help="new offset",       required=False)
    parser.add_argument('-m','--metric',    type=str, help="new metric type",  required=False, choices=['gauge', 'counter', None])
    parser.add_argument('-i','--iswritable',type=bool, help="is writable",     required=False, default=False)
    parser.add_argument('-e','--exposable', type=bool, help="to be exposed",   required=False, default=False)
    parser.add_argument('-l','--address',   type=str, help="new address",      required=False)
    parser.add_argument('-f','--filelog',   type=str, help="logfile",          required=False)
    parser.add_argument('-d','--dump',      type=int ,help="dump messages",    required=False, default=0)

    args = parser.parse_args()
    if args.filelog:
        logging.basicConfig(filename=args.filelog, encoding='utf-8', level=logging.DEBUG)
    if args.dump:
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    with open(args.jsonfile, 'r') as jsonfile:
        config_json = json.load(jsonfile)
    board     = args.board
    action    = args.action
    register  = args.register
    width     = args.width
    offset    = args.offset
    metric    = args.metric
    address  = args.address
    exposable = args.exposable
    writable  = args.iswritable
    if not board in config_json:
        board_list = ''
        for k in config_json.keys():
            board_list += '%s'%(k)
            if k != config_json.keys()[-1]:
                board_list +=', '
        logging.error('board %s is not defined. Avaliable boards are: %s'%(board, board_list))
        exit()
    logging.info('Action: %s\n'%(action))

    HandleRegisters(config_json, action, board, register, width=width, offset=offset, metric=metric, writable=writable, exposable=exposable, address=address, logging=logging)

    with open(args.jsonfile, 'w') as jsonfile:
        json.dump(config_json, jsonfile, indent=4)
    logging.info('Done')
