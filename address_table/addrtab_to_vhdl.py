#!/bin/env python3

import click
import json

vhdl_header = """library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package scouting_register_constants is
"""

vhdl_trailer = "end scouting_register_constants;"

@click.command()
@click.argument('addrtab', type=click.File('r'))
@click.option('-n', '--reg_name', default='moni_reg')
def main(addrtab, reg_name):
    at = json.load(addrtab)
    for board in at:
        registers = at[board]['registers']
        with open('decoder_constants_' + board + '.vhd', 'w') as f:
            f.write(vhdl_header)
            for reg in registers:
                reg_info = registers[reg]
                f.write('constant ' + reg + '_addr : natural := ' + str(int(reg_info['address'], 16)//4) + ';\n')
                f.write('constant ' + reg + '_offset : natural := ' + str(reg_info['offset']) + ';\n')
                f.write('constant ' + reg + '_width : natural := ' + str(reg_info['width']) + ';\n')
            f.write(vhdl_trailer)
        with open('monitoring_reg_template_' + board + '.vhd', 'w') as f:
            for reg in registers:
                reg_info = registers[reg]
                if reg_info['width'] == 1:
                    f.write(reg_name + '('+ reg + '_addr)(' + reg + '_offset) <= \n')
                else:
                    f.write(reg_name + '('+ reg + '_addr)(' + reg + '_width + ' + reg + '_offset  - 1 downto ' + reg + '_offset) <= \n')

if __name__ == "__main__":
    main()
