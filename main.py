import sys
import argparse
import math
import ctypes
import json
import logging
from ReadWriteTools import rw
actions = ["get", "set"]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()    #Python tool that makes it easy to create an user-friendly command-line interface                                                                                                                                                                               
    parser.add_argument('-j','--jsonfile', type=str, help="config file json", required=True)
    parser.add_argument('-b','--board',    type=str, help="scouting board",   required=True)
    parser.add_argument('-a','--action',   type=str, help="control action",   required=True, choices=actions)
    parser.add_argument('-r','--register', type=str, help="control register", required=True)
    parser.add_argument('-v','--value',    type=int, help="new value",        required=False)
    parser.add_argument('-f','--filelog',  type=str, help="logfile",          required=False)
    parser.add_argument('-d','--dump',     type=int ,help="dump messages",    required=False, default=0)
    args = parser.parse_args()
    
    if args.filelog:
        logging.basicConfig(filename=args.filelog, encoding='utf-8', format='%(levelname)s:%(message)s', level=logging.DEBUG)
    if args.dump:
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    with open(args.jsonfile, 'r') as jsonfile:
        config_json = json.load(jsonfile)

    board     = args.board
    reg       = args.register
    if not board in config_json:
        board_list = ''
        for k in config_json.keys():
            board_list += '%s'%(k)
            if k != config_json.keys()[-1]:
                board_list +=', '
        logging.error('board %s is not defined. Avaliable boards are: %s'%(board, board_list))
        exit()
        
    if not reg in config_json[board]["registers"]:
        reg_list = ''
        for k in config_json[board]["registers"].keys():
            reg_list += '%s'%(k)
            if k != config_json.keys()[-1]:
                board_list +=', '
        logging.error('register %s is not defined. Avaliable registers are: %s'%(reg, reg_list))
        exit()

    device     = str(config_json[board]["driver"])
    address   = config_json[board]["registers"][reg]["address"]
    offset     = config_json[board]["registers"][reg]["offset"]
    width      = config_json[board]["registers"][reg]["width"]
    action     = args.action
    value      = args.value
    
    verbose    = args.dump
    filelog    = args.filelog
    output     = 0
    # exceptions:
    if offset > 31:
        logging.error("Offset value %i is out of range 0-30. Check the configuration file."%(offset))
        exit()
    if width == 0:
        logging.error("Width must be grater than 0. Check the configuration file.")
    if offset+width > 32:
        logging.error("Mask ends out of range. Check the configuration file.")
        exit()

    if action == "set":
        if value == None:
            logging.error("action 'set' requires the optional argument --value (-v).")
            logging.info("board: %s (driver %s)"%(board, device))
            logging.info("register: %s"%(reg))
            logging.info("address: %s"%(address))
            logging.info("offset: %i"%(offset))
            logging.info("width: %i"%(width))
            logging.info("action: %s"%(action))
            exit()    

        nbits_allowed = width              # number of allowed bits
        nbits_write   = value.bit_length() # number of bits needed
        if nbits_write > nbits_allowed:
            logging.error("Overflow error! value %s doesn't fit in the allowed memory address."%(args.value))
            logging.info("board: %s (driver %s)"%(board, device))
            logging.info("register: %s"%(reg))
            logging.info("address: %s"%(address))
            logging.info("offset: %i"%(offset))
            logging.info("width: %i"%(width))
            logging.info("action: %s"%(action))
            exit()

        else:
            writevalue = ctypes.c_uint(value) #unsigned int (32 bits)       
            output  = rw.WriteProperty(device, address, offset, width, writevalue, verbose=verbose, logging=logging)
            if output == -1:
                logging.error('The value could not be updated.')
            else:
                logging.info('Set register %s from address %s on %s: %i'%(reg, address, board, writevalue.value))

    elif action == "get":
        output = rw.ReadProperty(device, address, offset, width, verbose=verbose, logging=logging)
        if output == -1:
            logging.error('The register could not be read.')
        else:
            logging.info('Get register %s from address %s on %s: %i'%(reg, address, board, output))

        logging.info("board: %s (driver %s)"%(board, device))
        logging.info("register: %s"%(reg))
        logging.info("address: %s"%(address))
        logging.info("offset: %i"%(offset))
        logging.info("width: %i"%(width))
        logging.info("action: %s"%(action))
        logging.info("output: %i"%(output))
