import requests
import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--register', type=str,  help="register",       required=True)
    parser.add_argument('-b', '--board',    type=str,  help="scouting board", required=True)
    parser.add_argument('-p', '--port',     type=str,  help="port",           required=False, default="8080")
    parser.add_argument('-v', '--value',    type=str,  help="value",          required=True)
    args = parser.parse_args()

    r= requests.post("http://127.0.0.1:%s/%s/%s/write"%(args.port, args.board, args.register), data={'value': args.value})
    print('Write request status:')
    print(r.status_code)
