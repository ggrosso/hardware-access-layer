#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

rm -rf env

python3.6 -m venv env
source env/bin/activate
    
python3.6 -m pip install --upgrade pip
python3.6 -m pip install --upgrade setuptools
python3.6 -m pip install -r requirements.txt  
#pip3 install python-ldap --force-reinstall --upgrade

