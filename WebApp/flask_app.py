import time
import flask

from gevent.pywsgi import WSGIServer
from gevent.pool import Pool

def RunApp_Flask(address_table, q_read, q_write, q_read_resp, q_write_resp, port="8080",address="0.0.0.0",  return_html=False):
    app = flask.Flask(__name__)
    def wait_for_queue(queue):
            while True:
                if  queue.empty():
                    time.sleep(1)
                    continue
                else:
                    msg = queue.get()
                    break
            return msg

    @app.route('/', methods=['GET'])
    def buttons():
        return flask.render_template('buttons.html',  boards=address_table, status=200)

    @app.route('/<board>/<register>/<action>', methods=['GET'])
    def manage_actions(board, register, action):
        if action=='write':
            return flask.render_template('write.html', board=board, register=register, status=200)
        elif action=='read':
            print('Read request on %s %s'%(board, register))
            q_read.put({'board':board, 'register':register})
            read_request_get = wait_for_queue(q_read_resp)
            value = read_request_get['value']
            status = read_request_get['status']
            message = read_request_get['message']
            print('got result of read request')
            print(read_request_get)
            if return_html:
                return flask.render_template('return.html', action=action, board=board, register=register, value=value, status=status, message=message)
            else:
                return read_request_get

    @app.route('/<board>/<register>/<action>', methods=['POST'])
    def back_from_writing(board, register, action, return_html=False):
        value = flask.request.form['value']
        if action=='write':
            print('Write request on:')
            print(board, register, value)
            q_write.put({'board':board, 'register':register, 'value':value})
            write_request_get = wait_for_queue(q_write_resp)
            status = write_request_get['status']
            message= write_request_get['message']
            print('got responce from write request')
            print(write_request_get)
            if return_html:
                return flask.render_template('return.html', action=action, board=board, register=register, value=value, status=status, message=message)
            else:
                return write_request_get

    try:
        pool = Pool(10)
        http_server = WSGIServer((address, int(port)), app, spawn=pool)
        http_server.serve_forever()
    except KeyboardInterrupt:
        return
