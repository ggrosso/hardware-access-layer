%define _prefix  /opt/l1scouting
%define _hconf   /etc/httpd/conf
%define _hconfd  /etc/httpd/conf.d
%define _systemd /etc/systemd/system
%define _logrotd /etc/logrotate.d
%define _homedir %{_prefix}/scone
%define _configf %{_homedir}/config.json
%define _configb %{_homedir}/old/scone.config.json
%define debug_package %{nil}

# To skip bytecompilation
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

Name: cms-scone
Version: %{_version}
Release: %{_release}
Summary: CMS L1 Scouting Hardware Access Layer API
Group: CMS/L1Scouting
License: GPL
Vendor: CMS/L1Scouting
Packager: %{_packager}
Source: %{name}.tar
ExclusiveOs: linux
Provides: cms-scone
Requires: python36

Prefix: %{_prefix}

%description
CMS L1 Scouting Hardware Access Layer API application with included Prometheus endpoint.

%files
%defattr(-,scouter,root,-)
%attr(-, scouter, root) %dir %{_prefix}
%attr(-, scouter, root) %dir %{_prefix}/logs
%attr(-, scouter, root) %dir %{_homedir}
%attr(-, scouter, root) %{_homedir}/*

%prep
%setup -c

%build

%install
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_prefix}
pwd
tar cf - . | (cd  $RPM_BUILD_ROOT%{_prefix}; tar xfp - )
mkdir -p $RPM_BUILD_ROOT%{_hconfd}
mkdir -p $RPM_BUILD_ROOT%{_systemd}
mkdir -p $RPM_BUILD_ROOT%{_logrotd}
mkdir -p $RPM_BUILD_ROOT%{_prefix}/logs
cd $RPM_BUILD_ROOT%{_homedir}/ReadWriteTools/
cc -fPIC -shared -o functions.so functions.c
cd $RPM_BUILD_ROOT%{_homedir}/address_table/

%clean
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT || :
%pre
# Backup config file if exists                                                                                                                                   
[ -f "%{_configf}" -o -L "%{_configf}" ] && mv "%{_configf}" "%{_configb}.`date +%Y-%m-%dT%T`" || :
%post
