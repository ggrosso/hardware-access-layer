import random
import json
import logging
import sys
import argparse
import math
import ctypes
import os
import time, datetime
import resource
import tracemalloc
import gc
import multiprocessing as mp
mp.set_start_method('fork')

from prometheus_client import start_http_server, Summary, Gauge, Counter
from ReadWriteTools import rw
from MonitoringTools import prometheus_monitoring
from WebApp import flask_app

###### memory debug tools
def memory_usage_resource():
    import resource
    rusage_denom = 1024.
    if sys.platform == 'darwin':
        # ... it seems that in OSX the output is different units ...
        rusage_denom = rusage_denom * rusage_denom
    mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / rusage_denom
    return mem

def dump_garbage(  ):
    """
    show us what the garbage is about
    """
    # Force collection
    print("\nGARBAGE:")
    gc.collect(  )

    print("\nGARBAGE OBJECTS:")
    for x in gc.garbage:
        s = str(x)
        if len(s) > 80: s = s[:77]+'...'
        print(type(x),"\n  ", s)
    return

###### control app process
def Control_process(address_table, q_read, q_write, q_read_resp, q_write_resp, port, address):
    return flask_app.RunApp_Flask(address_table, q_read, q_write, q_read_resp, q_write_resp, port=port, address=address)

###### parser formatter
def formatter(prog):
    return argparse.HelpFormatter(prog, max_help_position=10, width=100)

###### main
if __name__ == '__main__':
    gc.enable()
    # Start up the server to expose prmetheus metrics.  
    start_http_server(8097)
    parser = argparse.ArgumentParser( formatter_class=formatter)
    parser.add_argument('-j', '--jsonfile',  type=str,  help="config file json",         required=True)
    parser.add_argument('-l', '--logfile',   type=str,  help="logfile",                  required=False)
    parser.add_argument('-d', '--dump',      type=int,  help="dump messages",            required=False, default=0)
    parser.add_argument('-b', '--board',     type=str,  help="scouting board",           required=False)
    parser.add_argument('-e', '--errfile',   type=str,  help="debugfile",                required=False)
    parser.add_argument('-m', '--mem_debug', type=str, help="print memory tracing for debug", required=False, default=0)
    parser.add_argument('-t', '--timesleep', type=float,  help="seconds before updating",  required=False, default=5)
    parser.add_argument('-p', '--portwebapp',    type=str,    help="Web App port",            required=False, default="8080")
    parser.add_argument('-a', '--addresswebapp', type=str,    help="Web App address",         required=False, default="0.0.0.0")
    args = parser.parse_args()
    timesleep = args.timesleep
    port = args.portwebapp
    address = args.addresswebapp
    dump   = args.dump
    board  = args.board
    with open(args.jsonfile, 'r') as jsonfile:
        config_json = json.load(jsonfile)
    if args.logfile:
        logging.basicConfig(filename=args.logfile,
                            format='%(levelname)s:%(message)s', level=logging.INFO)
    if args.errfile:
        logging.basicConfig(filename=args.errfile,
                            format='%(levelname)s:%(message)s', level=logging.DEBUG)
        
    mem_debug = False
    if args.mem_debug:
        mem_debug = True
        tracemalloc.start()
        gc.set_debug(gc.DEBUG_LEAK)
        memory = 0
    verbose = 0
    if args.errfile: verbose = 1          
    del args, parser

    if board not in list(config_json.keys()) and not board==None:
        string_boards = ""
        for b in list(config_json.keys()): string_boards +='%s '%(b)
        logging.error("Board not recognised. List of available boards: %s"%(string_boards))
        exit()

    q_write, q_read, q_read_resp, q_write_resp = mp.Queue(), mp.Queue(), mp.Queue(), mp.Queue()
    p = mp.Process(target=Control_process, args=(config_json, q_read, q_write, q_read_resp, q_write_resp,port, address,))
    p.start()
    
    metrics_board_dict = {}
    previous_read_values_board = {}
    if board==None:
        logging.warning("The argument 'board' (-b) has not been specified. The monitoring process will loop over all the boards included in the address table.")
        for board in config_json:
            metrics_dict = {}
            for reg in config_json[board]["registers"]:
                metric   = config_json[board]["registers"][reg]["metric"]
                if metric == 'gauge':
                    metrics_dict[reg] = Gauge("%s_%s"%(board, reg), "%s_%s"%(board, reg))
                elif metric == 'counter':
                    metrics_dict[reg] = Counter("%s_%s"%(board, reg), "%s_%s"%(board, reg))

            metrics_board_dict[board]=metrics_dict
            previous_read_values_board[board] = {}
    else:
        metrics_dict = {}
        for reg in config_json[board]["registers"]:
            metric   = config_json[board]["registers"][reg]["metric"]
            if metric == 'gauge':
                metrics_dict[reg] = Gauge("%s_%s"%(board, reg), "%s_%s"%(board, reg))
            elif metric == 'counter':
                metrics_dict[reg] = Counter("%s_%s"%(board, reg), "%s_%s"%(board, reg))

        metrics_board_dict[board]=metrics_dict
        previous_read_values_board[board] = {}


    try:
        while True:
            # check for reading requests
            while not q_read.empty():
                read_request = q_read.get()
                status = "request received"
                if not read_request['register'] in config_json[read_request['board']]['registers']:
                    status   = "400"
                    message  = "bad request: register not available"
                    read_val = -1
                else:
                    board    = read_request['board']
                    register = read_request['register']
                    device   = config_json[board]["driver"] 
                    address  = config_json[board]["registers"][register]["address"]
                    width    = config_json[board]["registers"][register]["width"]
                    offset   = config_json[board]["registers"][register]["offset"]
                    read_val = rw.ReadProperty(device, address, offset, width, verbose=verbose, logging=logging)
                    if read_val==-1:
                        status  = "500"
                        message = 'Internal Server Error: an error occurred when trying to perform the ReadProperty function'
                    else:
                        status   = "200"
                        message  = 'success'
                q_read_resp.put({'value':read_val, 'status':status, 'message':message})
            # check for writing requests
            while not q_write.empty():
                write_request = q_write.get()
                if not write_request['register'] in config_json[write_request['board']]['registers']:
                    status    = "400"
                    message   = "bad request: register not available"
                    write_val = -1
                elif not config_json[write_request['board']]['registers'][write_request['register']]['writable']:
                    status    = "400"
                    message   = "bad request: register not writable"
                    write_val = -1
                else:
                    board     = write_request['board']
                    register  = write_request['register']
                    value     = write_request['value']
                    device    = config_json[board]["driver"]
                    address   = config_json[board]["registers"][register]["address"]
                    width     = config_json[board]["registers"][register]["width"]
                    offset    = config_json[board]["registers"][register]["offset"]
                    write_val = rw.WriteProperty(device, address, offset, width, int(value), logging=logging)
                    if write_val==-1:
                        status  = "500"
                        message = 'Internal Server Error: an error occurred when trying to perform the WriteProperty function'
                    else:
                        status  = "200"
                        message = 'success'
                q_write_resp.put({'value':write_request['value'], 'status':status, 'message':message})
            
            # monitoring metrics
            if board==None:
                for board in config_json:
                    device = str(config_json[board]["driver"])
                    logging.info("board %s (driver %s)"%(board, device))
                    prometheus_monitoring.monitoring_board(config_json, board, metrics_board_dict[board], device, previous_read_values=previous_read_values_board[board], dump=dump, verbose=verbose, logging=logging)
                gc.collect()
            else:
                device = str(config_json[board]["driver"])
                logging.info("board %s (driver %s)"%(board, device))
                prometheus_monitoring.monitoring_board(config_json, board, metrics_board_dict[board], device, previous_read_values=previous_read_values_board[board], dump=dump, verbose=verbose, logging=logging)
                gc.collect()

            if mem_debug:
                memory_avg, memory_max = tracemalloc.get_traced_memory()
                if memory_avg >memory:
                    print(datetime.datetime.now())
                    print(f"Current memory usage is {memory_avg / 10**6}MB; Peak was {memory_max / 10**6}MB")
                    memory = memory_avg

            time.sleep(timesleep)

    except KeyboardInterrupt:
        p.join()
        exit()
        tracemalloc.stop()
