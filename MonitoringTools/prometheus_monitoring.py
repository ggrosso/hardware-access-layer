from ReadWriteTools import rw
import gc
import logging
from prometheus_client import start_http_server, Summary, Gauge, Counter

def update_metric(metric, register, metric_type, value, board, previous_read_values, logging=None):
    if metric_type =='gauge':
        metric.set(value)
        return
    elif metric_type =='counter':
        previous_value = 0
        if register in previous_read_values.keys():
            previous_value = previous_read_values[register]
        if (value-previous_value)>=0:
            metric.inc(value-previous_value)
            del previous_value
            gc.collect()
            return 
        else:
            # counter overflow in the firmware or board reset (the present solution is approximated)
            metric.inc(value)
            del previous_value
            gc.collect()
            if logging !=None:
                logging.info('Counter %s on board %s has been reset'%(register, board))
            return 
    else:
        raise Exception()

def update_history(previous_values, register, value):
    previous_values[register] = value

def monitoring_board(config_json, board, metrics, device, previous_read_values, dump=0, verbose=False, logging=None):
    for reg in config_json[board]["registers"]:
        exposable = config_json[board]["registers"][reg]["exposable"]
        if not exposable: continue
        address = config_json[board]["registers"][reg]["address"]
        width    = config_json[board]["registers"][reg]["width"]
        offset   = config_json[board]["registers"][reg]["offset"]
        metric   = config_json[board]["registers"][reg]["metric"]
        read_val = rw.ReadProperty(device, address, offset, width, verbose=verbose, logging=logging)
        if logging !=None:
            logging.info("%s\tloc: %s\toffset: %s\twidth: %s\t--> value: %i (%s)"%(reg,address,offset,width,read_val,bin(read_val)))
        if dump:
            print("%s\tloc: %s\toffset: %s\twidth: %s\t--> value: %i (%s)"%(reg,address,offset,width,read_val,bin(read_val)))
        try:
            update_metric(metrics[reg], metric_type=metric, value=read_val, register=reg, board=board, previous_read_values=previous_read_values, logging=logging)
        except Exception as e:
            print("Unknown metric: %s; available metrics are 'gauge' and 'counter'"%(e))
        update_history(previous_values=previous_read_values, register=reg, value=read_val)
        del address, width, offset, metric, read_val
    gc.collect()
    return
