#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BUILD_DIR="${DIR}/scone_rpmbuild"
cd $DIR
echo "Directory $DIR"

VER=`git describe --tags | sed 's/^v//' | awk '{split($0,a,"-"); print a[1]}'`
REL=`git rev-list HEAD | wc -l | tr -d ' '`
TIM="`date -u +\"%F %T %Z\"`"
PCR="scouter" #"`logname`"


echo -n "{" > info.json
echo -n "\"version\":  \"$VER\"," >>info.json
echo -n "\"release\":  \"$REL\"," >>info.json
echo -n "\"time\":     \"$TIM\"," >>info.json
echo -n "\"packager\": \"$PCR\""  >>info.json
echo -n "}" >> info.json

echo "Version $VER, release $REL"

rm -fR ${BUILD_DIR}/SOURCES
mkdir -p ${BUILD_DIR}/SOURCES

mkdir -p ${BUILD_DIR}/SOURCES/scone
cp -R ${DIR}/env/lib/python3.6/site-packages ${BUILD_DIR}/SOURCES/scone
cp -R ${DIR}/{MonitoringTools,ReadWriteTools,WebApp,address_table,package} ${BUILD_DIR}/SOURCES/scone
cp -R ${DIR}/*.py ${BUILD_DIR}/SOURCES/scone
tar cf ${BUILD_DIR}/SOURCES/cms-scone.tar -C ${BUILD_DIR}/SOURCES scone
rm -fR ${BUILD_DIR}/SOURCES/scone

RPM_OPTS=(--define "_topdir $BUILD_DIR" --define "_version $VER" --define "_release $REL" --define "_packager $PCR")
rpmbuild "${RPM_OPTS[@]}" -bb package/package.spec
ls ${BUILD_DIR}/RPMS/x86_64/
cp ${BUILD_DIR}/RPMS/x86_64/cms-scone-* ./cms-scone_${VER}.rpm
echo cms-scone_${VER}.rpm
