import sys
import argparse
import math
import ctypes
import logging
import gc

so_file = "./ReadWriteTools/functions.so"
cfunc     = ctypes.CDLL(so_file)

def ReadProperty(device, address, offset, width, logging=None, verbose=False):
    device_c   = ctypes.c_char_p(device.encode('utf-8'))
    address_c = ctypes.c_char_p(address.encode('utf-8'))
    verbose_c  = ctypes.c_uint(verbose)
    read_32bits  = ctypes.c_uint(0)
    isRead       = cfunc.read_bits(device_c, address_c, ctypes.byref(read_32bits), verbose_c)
    if isRead == -1:
        logging.error("An error occured while executing C function read_bits")
        return -1
    string_32bits = "{:032b}".format(read_32bits.value)
    logging.debug("Read:  %s"%(string_32bits))
    mask   = 2**width - 1
    output = (read_32bits.value>>offset) & mask      # convert selected string to integer                                           
    del read_32bits, isRead, string_32bits, mask, device_c, address_c, verbose_c
    gc.collect()
    logging.debug("Unmasked result: %i (%s)"%(output, str(bin(output))))
    return output

def WriteProperty(device, address, offset, width, value, logging=None, verbose=False):
    device_c   = ctypes.c_char_p(device.encode('utf-8'))
    address_c = ctypes.c_char_p(address.encode('utf-8'))
    verbose_c  = ctypes.c_uint(verbose)
    read_value    = ctypes.c_uint(0);
    write_value   = ctypes.c_uint(value)
    isRead        = cfunc.read_bits(device_c, address_c, ctypes.byref(read_value), verbose_c)
    if isRead == -1:
        logging.error("An error occured while executing C function read_bits")
        return -1
    string_read   = "{:032b}".format(read_value.value)
    string_write  = "{:032b}".format(write_value.value<<offset)
    logging.debug("Read:  %s"%(string_read))
    logging.debug("Write: %s"%(string_write))
    mask      = (2**width-1)<<offset
    notmask   = (2**32-1)-mask
    output    = ((write_value.value<<offset) & mask) | (read_value.value & notmask)
    isWritten = cfunc.write_bits(device_c, address_c, output, verbose_c)
    if isWritten == -1:
        logging.error("An error occured while executing C function write_bits.")
        del read_value, isRead, string_read, string_write, mask, notmask, output, device_c, address_c, verbose_c
        gc.collect()
        return -1
    # Readback as checkout                                                                                                          
    isReadcheck = cfunc.read_bits(device_c, address_c, ctypes.byref(read_value), verbose_c )
    if isReadcheck == -1:
        logging.error("An error occured while executing C function read_bits")
        del read_value, isRead, isReadcheck, string_read, string_write, mask, notmask, output, device_c, address_c, verbose_c
        gc.collect()
        return -1
    if read_value.value == output:
        del read_value, isRead, isReadcheck, string_read, string_write, mask, notmask, output, device_c, address_c, verbose_c
        gc.collect()
        return 0
    else:
        logging.error("Readback failed. Register not writable.")
        logging.error("Check the reading permission for address %s on board %s"%(address, device))
        del read_value, isRead, isReadcheck, string_read, string_write, mask, notmask, output, device_c, address_c, verbose_c
        gc.collect()
        return -1
