# SCOUTING BOARD CONTROL AND MONITORING
40MHz scouting CMS

## Offline:

- main.py : python interface to get/set (r/rw) registers

            Arguments:

            -- jsonfile (-j)  required; configuration file containing the addess table (full path)
            -- board (-b):    required; choose ["board0", "board1", ...]
            -- register (-r): required; choose ["register0", "register1", ...]
            -- action (-a):   required; choose ["set", "get"]
            -- value (-v):    optional; new state for property if action = "set"
            -- filelog (-f):  optional; path to a .txt file; if not given no logfile is created
            -- dump (-d):     optional; choose 1 to print out messages on the terminal, 0 otherwise (default = 0)     
            
- functions.c : C functions to read/write in the board using AXI-lite from Xilinx (https://github.com/Xilinx/dma_ip_drivers/tree/master/XDMA/linux-kernel)

- addrtab_example.json : contains an address table with the following configuration

                'board_name1' : {
                                'driver' : '/...',
                                'registers' : {
                                                'register_name1' : {
                                                                    width : 32
                                                                    offset : 0
                                                                    address : '0x0'
                                                                    metric : 'gauge'
                                                },
                                                'register_name2' : {
                                                                    width : 32
                                                                    offset : 0
                                                                    address : '0x4'
                                                                    metric : 'counter'
                                                },
                                }
                }


            
### set up:
#### if you don't have a driver already:

 - download the Xlinx driver from https://github.com/Xilinx/dma_ip_drivers/            
 - and follow the instructions to install the Xlinx driver

 #### once the driver is mounted proceed as follows:

 - create a working directory and download the package:

            mkdir $HOME/WORKDIR  
            git clone https://gitlab.cern.ch/scouting-demonstrator/hardware-access-layer
                 
 - build the shared library:
           
            cd $HOME/WORKDIR/SCOUTING
            cc -fPIC -shared -o functions.so functions.c

- run main.py

           python main.py -j [json_config_file] -b [board_name] -a [set/get] -r [register_name] (-v [new_value] -f [txt_logfile] -d [0/1])

## Online

We use Prometheus metrics to monitor the registers output (for more details see Prometheus [documentation](https://prometheus.io)).
We use the official [python client](https://github.com/prometheus/client_python/)

#### Install Prometheus
- if not already, install Prometheus following the instructions at [https://prometheus.io/docs/prometheus/latest/installation/](https://prometheus.io/docs/prometheus/latest/installation/).
- install Prometheus python client (e.g. `pip install prometheus_client` )

- Note: prometheus is already installed on the machines at P5; the prometheus_client package is already install on the scoutdaq pc for python3.6

#### Run
- l1scout_monitoring.py : python interface for monitoring. It sets up a http server and use prometheus gauge to monitor dedicated registers on the scouting boards. The registers and boards are specified in the address table. 

            Arguments:

            -- jsonfile (-j)   required; configuration file containing the addess table (full path)
            -- board (-b):     optional; if none given, loop over all the boards in the address table 
            -- timesleep (-t): optional; monitoring rate in seconds (default = 5)
            -- logfile (-l):   optional; path to a .txt file; if not given no logfile is created
            -- errfile (-e):   optional; path to a .txt file; if not given no logfile is created
            -- dump (-d):      optional; choose 1 to print out messages on the terminal, 0 otherwise (default = 0) 

*Note:* the script is compatible with python2.7 and python3.6. On the scoutdaq pc the prometheus_client package is installed on python3.6.
- l1scout_monitoring_and_control.py: python interface for monitoring and control. It sets up a multiprocess manager that loop over the reading and writing requests and the monitoring request. The writing and reading requests can be sent through the web page interface (running on port 8080) or directly from terminal using the scripts Request_write.py and Request_read.py (for more details see below).

            Arguments:

            -- jsonfile (-j)   required; configuration file containing the addess table (full path)
            -- board (-b):     optional; if none given, loop over all the boards in the address table
            -- timesleep (-t): optional; monitoring rate in seconds (default = 5)
            -- logfile (-l):   optional; path to a .txt file; if not given no logfile is created
            -- errfile (-e):   optional; path to a .txt file; if not given no logfile is created
            -- dump (-d):      optional; choose 1 to print out messages on the terminal, 0 otherwise (default = 0)

*Note:* the script is compatible with python2.7 and python3.6. On the scoutdaq pc the prometheus_client package is installed on python3.6. The web application is built on Flask. On the scoutdaq pc Flask is currently available installing a rpm package. The path to the rpm package is included in the PYTHONPATH by running the following script:

  	    source setup_rpm.sh

- Request_read.py  python interface to  send an http GET request to read a given register on a board.

  	    Arguments:
            -- board (-b):    required; name of the acquisition board
	    -- register (-r): required; name of the register to be read
	    -- port (-p):     optional; port hosting the web application (default = "8080")

- Request_write.py: python interface to  send an http POST request to write on a given register on a board.

            Arguments:
            -- board (-b):    required; name of the acquisition board
            -- register(-r):  required; name of the register to be read
	    -- value (-v):    required; value to be written
            -- port (-p):     optional; port  hosting the web application (default = "8080")

